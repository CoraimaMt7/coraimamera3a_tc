﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoraimaM3A.Tercer_Nivel_Cia
{
    public class Cliente
    {
        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string numero_Cedula;

        public string Numero_Cedula
        {
            get { return numero_Cedula; }
            set { numero_Cedula = value; }
        }
        private string direccion;

        public string Direccion 
        {
            get { return direccion; }
            set { direccion = value; }
        }
        public void Datos_Clientes()
        {
            Console.WriteLine("Empresa Tercer Nivel Cía");

            Console.WriteLine("----------------------------");
            Console.WriteLine(" ");
            Console.WriteLine("Ingrese su Nombre");
            nombre = Console.ReadLine();
            Console.WriteLine("Ingrese su Apellido");
            apellido = Console.ReadLine();
            Console.WriteLine("Ingrese su Numero de Cedula");
            numero_Cedula = Console.ReadLine();
            Console.WriteLine("SU DIRECCION");
            direccion = Console.ReadLine(); 
        }

    }
}
