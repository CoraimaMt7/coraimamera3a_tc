﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoraimaM3A.Tercer_Nivel_Cia
{
    public class Gasolina
    {
        private double Total;
        private double IVA;
        private double Precio;
        public Gasolina()
        {
            Total = 0.0;
            IVA = 0.12;
            Precio = 0.0;
        }
        private string tipo;

        public string Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
        private int cantidad_Galones;

        public int Cantidad_Galones
        {
            get { return cantidad_Galones; }
            set { cantidad_Galones = value; }
        }
        public void Precio_Venta()
        {
            if (tipo == "EXTRA")
            {
                Precio = 1.50 * cantidad_Galones;
            }
            else
            {
                if (tipo == "SUPER")
                {
                    Precio = 2.00 * cantidad_Galones;
                }
            }
            IVA = Precio * 0.12;
            Total = IVA + Precio;
        }
        public void Mostrar()
        {
            Console.WriteLine("subtotal:               " + Precio);
            Console.WriteLine("IVA:                    " + IVA);
            Console.WriteLine("Total a Pagar:          " + Total);
            Console.WriteLine(" ");
            Console.WriteLine("----Gracias por su Compra----");
        }
        public void Datos_Gasolina()
        {
            Console.WriteLine("Tipo de Gasolina: EXTRA o SUPER");
            tipo = Console.ReadLine();
            Console.WriteLine("Ingrese La Cantidad De Galones");
            Cantidad_Galones = int.Parse(Console.ReadLine());

        }
    }
}

