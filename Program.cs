﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoraimaM3A.Tercer_Nivel_Cia;

namespace CoraimaM3A
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Gasolina Chevron = new Gasolina();
            Cliente Persona = new Cliente();
            Persona.Datos_Clientes();
            Chevron.Datos_Gasolina();

            Console.WriteLine("Nombres:                " + Persona.Nombre);
            Console.WriteLine("Apellido:               " + Persona.Apellido);
            Console.WriteLine("Numero de cedula:       " + Persona.Numero_Cedula);
            Console.WriteLine("Direccion:              " + Persona.Direccion);
            Console.WriteLine("Tipo de gasolina:       " + Chevron.Tipo);
            Console.WriteLine("Cantidad de gasolina:   " + Chevron.Cantidad_Galones);

            Chevron.Precio_Venta();
            Chevron.Mostrar();
            Console.ReadKey();
        }
    }
}
